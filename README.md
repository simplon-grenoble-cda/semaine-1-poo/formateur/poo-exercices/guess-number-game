Nous allons construire un jeu simple en ligne de commande. Le programme détermine au hasard un nombre entre 0 et 100
qu'il garde secret.

L'utilisateur est invité à deviner le nombre en question. Il propose un nombre, et le jeu lui dit si le nombe à trouver est
inférieur ou supérieur au nombre proposé.

Si l'utilisateur propose le nombre exact, il a gagné.

Le but est de trouver en faisant le moins de tenatives. Le jeu indique à l'utilisateur son nombre de tentatives à la fin.


Note : attention à gérer le cas où l'utilisateur rentre quelque chose d'invalide dans la console lorsqu'on lui demande un nombre.
